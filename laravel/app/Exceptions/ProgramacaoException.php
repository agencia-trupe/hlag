<?php

namespace App\Exceptions;

use Exception;

class ProgramacaoException extends Exception
{
    private $validationErrors;

    public function __construct($message, $code = 0, Exception $previous = null, $validationErrors = [])
    {
        parent::__construct($message, $code, $previous);

        $this->validationErrors = $validationErrors;
    }

    public function getValidationErrors()
    {
        return $this->validationErrors;
    }
}
