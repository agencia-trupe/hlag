<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Programacao;
use App\Models\Servico;
use App\Models\Terminal;
use App\Models\ContatoDivisao;

use App\Models\ProgramacaoExportacao;

use App\Helpers\Tools;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    public function index()
    {
        $programacao = Programacao::all();

        $portos   = $programacao->groupBy('porto')->keys()->toArray();
        $servicos = $programacao->groupBy('servico')->keys()->toArray();
        $navios   = $programacao->groupBy('navio')->keys()->toArray();

        return view('frontend.home', compact(
            'programacao', 'portos', 'servicos', 'navios'
        ));
    }

    public function consulta()
    {
        $programacao = Programacao::all();

        if (! count($programacao))
            return redirect()->route('home');

        $resultados = $this->filtraProgramacao();

        return view('frontend.consulta', compact('programacao', 'resultados'));
    }

    public function consultaExportacao($extension)
    {
        if (! in_array($extension, ['xls', 'pdf']))
            return redirect()->route('home');

        $resultados = $this->filtraProgramacao();

        if (! $resultados->count())
            return redirect()->route('home');

        $exportacao = new ProgramacaoExportacao($resultados);

        if ($extension == 'xls') {
            $exportacao->downloadXls();
        } elseif ($extension == 'pdf') {
            $exportacao->downloadPdf();
        }
    }

    public function servicos()
    {
        $links = Servico::ordenados()->get();

        return view('frontend.servicos', compact('links'));
    }

    public function terminais()
    {
        $links = Terminal::ordenados()->get();

        return view('frontend.terminais', compact('links'));
    }

    public function contatos()
    {
        $divisoes = ContatoDivisao::ordenados()->get();

        return view('frontend.contatos', compact('divisoes'));
    }

    private function filtraProgramacao()
    {
        $resultados = Programacao::query();

        foreach(['porto', 'servico', 'navio'] as $campo) {
            if ($valor = request($campo))
                $resultados->where($campo, 'LIKE', "%$valor%");
        }

        $resultados = $resultados->get();

        foreach(['previsao_atracacao', 'previsao_saida'] as $campo) {
            $inicio = request($campo.'_inicio');
            $fim    = request($campo.'_fim');

            if ($inicio && Tools::dataValidaQuery($inicio))
                $resultados = $resultados->filter(function($p) use ($campo, $inicio) {
                    return $p->{$campo}->format('m-d') >= Tools::formataDataQuery($inicio);
                });

            if ($fim && Tools::dataValidaQuery($fim))
                $resultados = $resultados->filter(function($p) use ($campo, $fim) {
                    return $p->{$campo}->format('m-d') <= Tools::formataDataQuery($fim);
                });
        }

        if (in_array($deadline = request('deadline'), [
            'dca', 'mdgf', 'draft', 'carga_liberacao', 'vgm'
        ])) {
            $inicio = request('deadline_inicio');
            $fim    = request('deadline_fim');

            if ($inicio && Tools::dataValidaQuery($inicio))
                $resultados = $resultados->filter(function($p) use ($deadline, $inicio) {
                    return $p->{'deadline_'.$deadline}->format('m-d') >= Tools::formataDataQuery($inicio);
                });

            if ($fim && Tools::dataValidaQuery($fim))
                $resultados = $resultados->filter(function($p) use ($deadline, $fim) {
                    return $p->{'deadline_'.$deadline}->format('m-d') <= Tools::formataDataQuery($fim);
                });
        }

        return $resultados;
    }
}
