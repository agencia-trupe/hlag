<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosEmailsRequest;
use App\Http\Controllers\Controller;

use App\Models\ContatoDivisao;
use App\Models\ContatoEmail;

class ContatosEmailsController extends Controller
{
    public function index(ContatoDivisao $divisao)
    {
        $registros = $divisao->emails;

        return view('painel.contatos.emails.index', compact('divisao', 'registros'));
    }

    public function create(ContatoDivisao $divisao)
    {
        return view('painel.contatos.emails.create', compact('divisao'));
    }

    public function store(ContatoDivisao $divisao, ContatosEmailsRequest $request)
    {
        try {

            $input = $request->all();

            $divisao->emails()->create($input);

            return redirect()->route('painel.contatos.emails.index', $divisao->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ContatoDivisao $divisao, ContatoEmail $registro)
    {
        return view('painel.contatos.emails.edit', compact('divisao', 'registro'));
    }

    public function update(ContatoDivisao $divisao, ContatoEmail $registro, ContatosEmailsRequest $request)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.contatos.emails.index', $divisao->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ContatoDivisao $divisao, ContatoEmail $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.contatos.emails.index', $divisao->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
