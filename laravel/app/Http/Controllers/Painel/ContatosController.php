<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRequest;
use App\Http\Controllers\Controller;

use App\Models\ContatoDivisao;

class ContatosController extends Controller
{
    public function index()
    {
        $registros = ContatoDivisao::ordenados()->get();

        return view('painel.contatos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.contatos.create');
    }

    public function store(ContatosRequest $request)
    {
        try {

            $input = $request->all();

            ContatoDivisao::create($input);

            return redirect()->route('painel.contatos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ContatoDivisao $registro)
    {
        return view('painel.contatos.edit', compact('registro'));
    }

    public function update(ContatosRequest $request, ContatoDivisao $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.contatos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ContatoDivisao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.contatos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
