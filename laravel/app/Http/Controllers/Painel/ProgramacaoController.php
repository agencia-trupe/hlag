<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Programacao;
use App\Models\ProgramacaoHistorico;
use App\Models\ProgramacaoImportacao;

use App\Exceptions\ProgramacaoException;

class ProgramacaoController extends Controller
{
    public function index()
    {
        $programacao = Programacao::all();

        return view('painel.programacao.index', compact('programacao'));
    }

    public function importar(Request $request)
    {
        if (! $request->hasFile('arquivo')) {
            return back()->withErrors(['Erro ao importar programação: Arquivo não encontrado.']);
        }

        if (! in_array(
            strtolower($request->arquivo->getClientOriginalExtension()),
            ['xls', 'xlsx']
        )) {
            return back()->withErrors(['Erro ao importar programação: Arquivo inválido, o arquivo deve ser do tipo XLS ou XLSX.']);
        }

        try {

            $importacao = new ProgramacaoImportacao($request->arquivo);
            $importacao->importar();

            return redirect()->route('painel.programacao.index')->with('success', 'Programação importada com sucesso.');

        } catch (ProgramacaoException $e) {

            return back()->withErrors(
                array_merge(
                    ['Erro ao importar programação:'],
                    $e->getValidationErrors()
                )
            );

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao importar programação: '.$e->getMessage()]);

        }
    }

    public function limpar()
    {
        try {

            Programacao::truncate();

            return redirect()->route('painel.programacao.index')->with('success', 'Programação excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir programação: '.$e->getMessage()]);

        }
    }

    public function historico()
    {
        $historico = ProgramacaoHistorico::orderBy('created_at', 'DESC')
            ->paginate(15);

        return view('painel.programacao.historico', compact('historico'));
    }

    public function historicoDownload(ProgramacaoHistorico $historico)
    {
        $fileName = $historico->arquivo;

        if (! file_exists($file = storage_path(
            'app/historico-programacao/'.$fileName
        ))) {
            return response('File not found.', 404);
        }

        return response()->download($file);
    }
}
