<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TerminaisDeAtracacaoRequest;
use App\Http\Controllers\Controller;

use App\Models\Terminal;

class TerminaisDeAtracacaoController extends Controller
{
    public function index()
    {
        $registros = Terminal::ordenados()->get();

        return view('painel.terminais-de-atracacao.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.terminais-de-atracacao.create');
    }

    public function store(TerminaisDeAtracacaoRequest $request)
    {
        try {

            $input = $request->all();

            Terminal::create($input);

            return redirect()->route('painel.terminais-de-atracacao.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Terminal $registro)
    {
        return view('painel.terminais-de-atracacao.edit', compact('registro'));
    }

    public function update(TerminaisDeAtracacaoRequest $request, Terminal $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.terminais-de-atracacao.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Terminal $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.terminais-de-atracacao.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
