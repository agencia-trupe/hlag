<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PortosRequest;
use App\Http\Controllers\Controller;

use App\Models\Porto;

class PortosController extends Controller
{
    public function index()
    {
        $registros = Porto::get();

        return view('painel.portos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.portos.create');
    }

    public function store(PortosRequest $request)
    {
        try {

            $input = $request->all();

            Porto::create($input);

            return redirect()->route('painel.portos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Porto $registro)
    {
        return view('painel.portos.edit', compact('registro'));
    }

    public function update(PortosRequest $request, Porto $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.portos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Porto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.portos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
