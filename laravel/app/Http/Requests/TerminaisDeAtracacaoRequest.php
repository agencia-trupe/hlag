<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TerminaisDeAtracacaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'link' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
