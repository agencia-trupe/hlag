<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Route;

class PortosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $currentId = Route::current()->portos
            ? Route::current()->portos->id
            : null;

        $rules = [
            'sigla' => 'required|unique:portos,sigla,'.$currentId,
            'titulo' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function attributes()
    {
        return ['titulo' => 'título'];
    }
}
