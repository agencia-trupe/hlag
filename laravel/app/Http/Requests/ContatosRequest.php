<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'telefones_pt' => 'required',
            'telefones_en' => 'required',
            'telefones_es' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
