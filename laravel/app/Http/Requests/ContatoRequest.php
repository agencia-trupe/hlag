<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razao_social_pt' => 'required',
            'razao_social_en' => 'required',
            'razao_social_es' => 'required',
            'endereco_pt'     => 'required',
            'endereco_en'     => 'required',
            'endereco_es'     => 'required',
            'telefone_pt'     => 'required',
            'telefone_en'     => 'required',
            'telefone_es'     => 'required',
            'aviso_pt'        => 'required',
            'aviso_en'        => 'required',
            'aviso_es'        => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'razao_social_pt' => 'razão social',
            'endereco_pt'     => 'endereço',
            'aviso_pt'        => 'aviso'
        ];
    }
}
