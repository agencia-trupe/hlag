<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('consulta', 'HomeController@consulta')->name('consulta');
    Route::get('consulta/{extension}', 'HomeController@consultaExportacao')->name('consulta.exportacao');
    Route::get('servicos', 'HomeController@servicos')->name('servicos');
    Route::get('terminais-de-atracacao', 'HomeController@terminais')->name('terminais');
    Route::get('contatos', 'HomeController@contatos')->name('contatos');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['pt', 'en', 'es'])) {
            Session::put('locale', $idioma);
        }
        return redirect()->back();
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::get('programacao', 'ProgramacaoController@index')->name('painel.programacao.index');
		Route::post('programacao/importar', 'ProgramacaoController@importar')->name('painel.programacao.importar');
		Route::get('programacao/limpar', 'ProgramacaoController@limpar')->name('painel.programacao.limpar');
        Route::get('programacao/historico', 'ProgramacaoController@historico')->name('painel.programacao.historico');
        Route::get('programacao/historico/{historico}/download', 'ProgramacaoController@historicoDownload')->name('painel.programacao.historico.download');
		Route::resource('portos', 'PortosController');
		Route::resource('contatos', 'ContatosController');
		Route::resource('contatos.emails', 'ContatosEmailsController');
		Route::resource('terminais-de-atracacao', 'TerminaisDeAtracacaoController');
		Route::resource('servicos', 'ServicosController');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        // Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        // Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
