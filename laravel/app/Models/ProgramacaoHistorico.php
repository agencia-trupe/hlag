<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramacaoHistorico extends Model
{
    protected $table = 'programacao_historico';

    protected $guarded = ['id'];
}
