<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Dompdf\Dompdf;

class ProgramacaoExportacao
{
    private $resultados;

    public function __construct(Collection $resultados)
    {
        $this->resultados = $resultados;
    }

    public function downloadPdf()
    {
        $data     = ['resultados' => $this->resultados];
        $fileName = 'HapagLloyd-Consulta-'.date('d-m-Y').'.pdf';

        $dompdf = new Dompdf();
        $dompdf->loadHtml(view('frontend.consulta-pdf', $data)->render());
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream($fileName);
    }

    public function downloadXls()
    {
        $data     = $this->dadosFormatadosExcel();
        $fileName = 'HapagLloyd-Consulta-'.date('d-m-Y');

        Excel::create($fileName, function ($excel) use ($data) {
            $excel->sheet('rotas', function ($sheet) use ($data) {
                $sheet->fromModel($data);
                $sheet->row(1, function($row) {
                    $row->setBackground('#eeeeee');
                });
                $sheet->getStyle('A1:M1')->applyFromArray([
                    'font' => [
                        'size' => 9, //'9pt',
                        'bold' => true,
                        'color' => ['rgb' => '#23467d']
                    ],
                ]);
                $sheet->getDefaultRowDimension()->setRowHeight(20);
            });
        })->download('xls');
    }

    private function dadosFormatadosExcel()
    {
        $campos = ['porto', 'navio', 'viagem', 'servico', 'previsao_atracacao', 'previsao_saida', 'deadline_dca', 'deadline_mdgf', 'deadline_draft', 'deadline_carga_liberacao', 'deadline_vgm', 'terminal', 'pernada'];

        return $this->resultados->map(function($programacao) use ($campos) {
            $row = [];
            foreach($campos as $campo) {
                $label = mb_strtoupper(t('programacao.'.$campo));
                if (preg_match('/^previsao_/', $campo)) {
                    $row[$label] = $programacao->{$campo}->format('d/m');
                } elseif (preg_match('/^deadline_/', $campo)) {
                    $row[$label] = $programacao->{$campo}->format('d/m H:i');
                } else {
                    $row[$label] = $programacao->{$campo};
                }
            }
            return $row;
        });
    }
}