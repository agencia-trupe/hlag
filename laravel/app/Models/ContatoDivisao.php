<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ContatoDivisao extends Model
{
    protected $table = 'contatos_divisoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function emails()
    {
        return $this->hasMany(ContatoEmail::class, 'divisao_id')->ordenados();
    }
}
