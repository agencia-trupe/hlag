<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Porto extends Model
{
    protected $table = 'portos';

    protected $guarded = ['id'];
}
