<?php

use Illuminate\Database\Seeder;

class PlaceholderSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos')->insert([
            'titulo_pt' => 'Hapag-Lloyd',
            'titulo_en' => 'Hapag-Lloyd',
            'titulo_es' => 'Hapag-Lloyd',
            'link' => 'https://www.hapag-lloyd.com'
        ]);

        DB::table('terminais_de_atracacao')->insert([
            'titulo_pt' => 'Hapag-Lloyd',
            'titulo_en' => 'Hapag-Lloyd',
            'titulo_es' => 'Hapag-Lloyd',
            'link' => 'https://www.hapag-lloyd.com'
        ]);

        DB::table('contatos_divisoes')->insert([
            'titulo_pt' => 'Customer Service',
            'titulo_en' => 'Customer Service',
            'titulo_es' => 'Customer Service',
            'telefones_pt' => 'Phone 4090 1555 / 0300 11 55 300',
            'telefones_en' => 'Phone 4090 1555 / 0300 11 55 300',
            'telefones_es' => 'Phone 4090 1555 / 0300 11 55 300',
        ]);

        DB::table('contatos_emails')->insert([
            'divisao_id' => 1,
            'titulo_pt' => 'Sales Support',
            'titulo_en' => 'Sales Support',
            'titulo_es' => 'Sales Support',
            'email' => 'sales.br@hlag.com',
        ]);

        DB::table('portos')->insert([
            [
                'sigla' => 'BRSSZ',
                'titulo' => 'SANTOS',
            ],
            [
                'sigla' => 'BRRIO',
                'titulo' => 'RIO DE JANEIRO',
            ],
            [
                'sigla' => 'BRSSA',
                'titulo' => 'SALVADOR',
            ],
        ]);
    }
}
