<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'razao_social_pt' => 'Libra Serviços de Navegação Ltda c/o Hapag-Lloyd AG<br />CNPJ: 42.581.413.0001-57',
            'razao_social_en' => 'Libra Serviços de Navegação Ltda c/o Hapag-Lloyd AG<br />CNPJ: 42.581.413.0001-57',
            'razao_social_es' => 'Libra Serviços de Navegação Ltda c/o Hapag-Lloyd AG<br />CNPJ: 42.581.413.0001-57',
            'endereco_pt' => 'Av. das Nações Unidas 14261 - Ala B - 28º andar - Bloco B<br />Vila Girtrudes - São Paulo/SP - 04794-000',
            'endereco_en' => 'Av. das Nações Unidas 14261 - Ala B - 28º andar - Bloco B<br />Vila Girtrudes - São Paulo/SP - 04794-000',
            'endereco_es' => 'Av. das Nações Unidas 14261 - Ala B - 28º andar - Bloco B<br />Vila Girtrudes - São Paulo/SP - 04794-000',
            'telefone_pt' => '+55 11 4090-1555',
            'telefone_en' => '+55 11 4090-1555',
            'telefone_es' => '+55 11 4090-1555',
            'aviso_pt' => 'A Hapag-Lloyd reserva-se o direito de alterar os dados aqui divulgados sem prévio aviso.',
            'aviso_en' => 'A Hapag-Lloyd reserva-se o direito de alterar os dados aqui divulgados sem prévio aviso.',
            'aviso_es' => 'A Hapag-Lloyd reserva-se o direito de alterar os dados aqui divulgados sem prévio aviso.',
        ]);
    }
}
