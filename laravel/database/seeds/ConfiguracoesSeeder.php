<?php

use Illuminate\Database\Seeder;

class ConfiguracoesSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title' => 'Hapag-Lloyd',
            'description' => '',
            'keywords' => '',
            'imagem_de_compartilhamento' => '',
            'analytics' => '',
        ]);
    }
}
