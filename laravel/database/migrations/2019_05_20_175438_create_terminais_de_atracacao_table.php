<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminaisDeAtracacaoTable extends Migration
{
    public function up()
    {
        Schema::create('terminais_de_atracacao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('terminais_de_atracacao');
    }
}
