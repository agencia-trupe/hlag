<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramacaoTable extends Migration
{
    public function up()
    {
        Schema::create('programacao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('porto');
            $table->string('navio');
            $table->string('viagem');
            $table->string('servico');
            $table->string('previsao_atracacao');
            $table->string('previsao_saida');
            $table->string('deadline_dca');
            $table->string('deadline_mdgf');
            $table->string('deadline_draft');
            $table->string('deadline_carga_liberacao');
            $table->string('deadline_vgm');
            $table->string('terminal');
            $table->string('pernada');
            $table->timestamps();
        });

        Schema::create('programacao_historico', function (Blueprint $table) {
            $table->increments('id');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('programacao_historico');
        Schema::drop('programacao');
    }
}
