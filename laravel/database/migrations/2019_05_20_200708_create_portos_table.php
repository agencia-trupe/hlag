<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortosTable extends Migration
{
    public function up()
    {
        Schema::create('portos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sigla')->unique();
            $table->string('titulo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('portos');
    }
}
