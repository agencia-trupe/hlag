<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatosTable extends Migration
{
    public function up()
    {
        Schema::create('contatos_divisoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('telefones_pt');
            $table->string('telefones_en');
            $table->string('telefones_es');
            $table->timestamps();
        });

        Schema::create('contatos_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('divisao_id')->unsigned()->nullable();
            $table->foreign('divisao_id')->references('id')->on('contatos_divisoes')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('email');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contatos_emails');
        Schema::drop('contatos_divisoes');
    }
}
