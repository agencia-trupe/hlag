<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->text('razao_social_pt');
            $table->text('razao_social_en');
            $table->text('razao_social_es');
            $table->text('endereco_pt');
            $table->text('endereco_en');
            $table->text('endereco_es');
            $table->string('telefone_pt');
            $table->string('telefone_en');
            $table->string('telefone_es');
            $table->string('aviso_pt');
            $table->string('aviso_en');
            $table->string('aviso_es');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contato');
    }
}
