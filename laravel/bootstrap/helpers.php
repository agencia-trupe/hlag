<?php

function t($termo) {
    return trans('frontend.'.$termo);
}

function tobj($obj, $termo)
{
    return $obj->{$termo.'_'.app()->getLocale()};
}
