@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="center">
            <h1>{{ t('nav.terminais') }}</h1>

            <div class="links">
                @foreach($links as $link)
                <a href="{{ $link->link }}" target="_blank">
                    {{ tobj($link, 'titulo') }}
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
