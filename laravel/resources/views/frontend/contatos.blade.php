@extends('frontend.common.template')

@section('content')

    <div class="main">
        <div class="center">
            <h1>{{ t('nav.contatos') }}</h1>

            @foreach($divisoes as $divisao)
            <div class="contatos-divisao">
                <h2>{{ tobj($divisao, 'titulo') }}</h2>
                <h3>{{ tobj($divisao, 'telefones') }}</h3>
                <div class="emails">
                    @foreach($divisao->emails as $email)
                    <div class="email">
                        <span>{{ tobj($email, 'titulo') }}:</span>
                        <a href="mailto:{{ $email->email }}">
                            {{ $email->email }}
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
