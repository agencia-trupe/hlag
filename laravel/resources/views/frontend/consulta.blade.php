@extends('frontend.common.template')

@section('content')

    <div class="main programacao">
        <div class="center">
            <h1>{{ t('programacao.consulte') }}</h1>

            <div class="selecione-divulgacao">
                <p>{{ t('programacao.selecione') }}</p>
                <p>
                    {{ t('programacao.divulgado') }}
                    {{ $programacao->last()->created_at->format('d/m/Y') }}
                </p>
            </div>

            <div class="opcoes-consulta">
                <a href="{{ route('home', $_GET) }}">
                    {{ t('programacao.opcoes-consulta') }}
                </a>
            </div>
        </div>
    </div>

    <div class="consulta">
        <div class="center">
            @if(count($resultados))
            <div class="table-wrapper">
                <table>
                    <thead>
                        <tr>
                            <th>{{ t('programacao.porto') }}</th>
                            <th>{{ t('programacao.navio') }}</th>
                            <th>{{ t('programacao.viagem') }}</th>
                            <th>{{ t('programacao.servico') }}</th>
                            <th>{{ t('programacao.previsao_atracacao') }}</th>
                            <th>{{ t('programacao.previsao_saida') }}</th>
                            <th>{{ t('programacao.deadline_dca') }}</th>
                            <th>{{ t('programacao.deadline_mdgf') }}</th>
                            <th>{{ t('programacao.deadline_draft') }}</th>
                            <th>{{ t('programacao.deadline_carga_liberacao') }}</th>
                            <th>{{ t('programacao.deadline_vgm') }}</th>
                            <th>{{ t('programacao.terminal') }}</th>
                            <th>{{ t('programacao.pernada') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($resultados as $resultado)
                        <tr>
                            <td>{{ $resultado->porto }}</td>
                            <td>{{ $resultado->navio }}</td>
                            <td>{{ $resultado->viagem }}</td>
                            <td>{{ $resultado->servico }}</td>
                            <td>{{ $resultado->previsao_atracacao->format('d/m') }}</td>
                            <td>{{ $resultado->previsao_saida->format('d/m') }}</td>
                            <td>{{ $resultado->deadline_dca->format('d/m H:i') }}</td>
                            <td>{{ $resultado->deadline_mdgf->format('d/m H:i') }}</td>
                            <td>{{ $resultado->deadline_draft->format('d/m H:i') }}</td>
                            <td>{{ $resultado->deadline_carga_liberacao->format('d/m H:i') }}</td>
                            <td>{{ $resultado->deadline_vgm->format('d/m H:i') }}</td>
                            <td>{{ $resultado->terminal }}</td>
                            <td>{{ $resultado->pernada }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="exportar">
                <a href="{{ route('consulta.exportacao', array_merge(['xls'], $_GET)) }}" class="exportar-xls">
                    {{ t('programacao.exportar-xls') }}
                </a>
                <a href="{{ route('consulta.exportacao', array_merge(['pdf'], $_GET)) }}" class="exportar-pdf">
                    {{ t('programacao.exportar-pdf') }}
                </a>
            </div>
            @else
            <p>{{ t('programacao.nenhum-resultado') }}</p>
            @endif
        </div>
    </div>

@endsection
