<a href="{{ route('home') }}" @if(Tools::routeIs(['home', 'consulta'])) class="active" @endif>{{ t('nav.programacao') }}</a>
<a href="{{ route('servicos') }}" @if(Tools::routeIs('servicos')) class="active" @endif>{{ t('nav.servicos') }}</a>
<a href="{{ route('terminais') }}" @if(Tools::routeIs('terminais')) class="active" @endif>{{ t('nav.terminais') }}</a>
<a href="{{ route('contatos') }}" @if(Tools::routeIs('contatos')) class="active" @endif>{{ t('nav.contatos') }}</a>
