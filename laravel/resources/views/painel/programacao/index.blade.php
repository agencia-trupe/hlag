@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Programação
            <div class="btn-group pull-right">
                <a href="{{ route('painel.programacao.historico') }}" class="btn btn-sm btn-warning">
                    <span class="glyphicon glyphicon-time" style="margin-right:10px"></span>
                    Histórico
                </a>
                <a href="{{ route('painel.portos.index') }}" class="btn btn-sm btn-info">
                    <span class="glyphicon glyphicon-th-list" style="margin-right:10px"></span>
                    Editar Portos
                </a>
            </div>
        </h2>
    </legend>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Importar Arquivo</h4>
        </div>
        <div class="panel-body">
            {!! Form::open(['route' => 'painel.programacao.importar', 'files' => true]) !!}
                <div class="input-group">
                    {!! Form::file('arquivo', ['class' => 'form-control', 'required' => true]) !!}
                    <span class="input-group-btn">
                        {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
                    </span>
                </div>
            {!! Form::close() !!}

            @if(count($programacao))
            <div class="alert alert-info" style="margin:10px 0 0">
                <span class="glyphicon glyphicon-info-sign" style="margin-right: 10px;font-size:1.1em"></span>
                Ao importar um novo arquivo todos os registros abaixo serão substituídos.
            </div>
            @endif
        </div>
        @if(count($programacao))
        <div class="panel-footer">
            <div class="btn-group btn-block">
                <a href="{{ route('painel.programacao.limpar') }}" class="btn btn-danger btn-sm btn-delete btn-delete-link btn-delete-multiple">
                    <span class="glyphicon glyphicon-trash" style="margin-right:5px"></span>
                    Limpar Programação
                </a>
            </div>
        </div>
        @endif
    </div>

    @if(!count($programacao))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <div class="well">
        Divulgado em:
        <strong>{{ $programacao->last()->created_at->format('d/m/Y') }}</strong>
    </div>

    <style>
        .table-wrapper {
            width: 100%;
            overflow: auto;
        }
        .table-wrapper table {
            min-width: 1400px;
            margin-bottom: 0;
        }
    </style>
    </div><div class="container-fluid" style="margin:-30px auto 30px;max-width:1600px">
    <div class="table-wrapper">
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Porto</th>
                <th>Navio</th>
                <th>Viagem</th>
                <th>Serviço</th>
                <th>Previsão de Atracação</th>
                <th>Previsão de Saída</th>
                <th>Deadline de DCA</th>
                <th>Deadline de MDGF</th>
                <th>Deadline de Draft</th>
                <th>Deadline de Carga/Liberação</th>
                <th>VGM</th>
                <th>Terminal</th>
                <th>Pernada</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($programacao as $registro)
            <tr class="tr-row">
                <td>{{ $registro->porto }}</td>
                <td>{{ $registro->navio }}</td>
                <td>{{ $registro->viagem }}</td>
                <td>{{ $registro->servico }}</td>
                <td style="white-space:nowrap">
                    {{ $registro->previsao_atracacao->format('d/m') }}
                </td>
                <td style="white-space:nowrap">
                    {{ $registro->previsao_saida->format('d/m') }}
                </td>
                <td style="white-space:nowrap">
                    {{ $registro->deadline_dca->format('d/m H:i') }}
                </td>
                <td style="white-space:nowrap">
                    {{ $registro->deadline_mdgf->format('d/m H:i') }}
                </td>
                <td style="white-space:nowrap">
                    {{ $registro->deadline_draft->format('d/m H:i') }}
                </td>
                <td style="white-space:nowrap">
                    {{ $registro->deadline_carga_liberacao->format('d/m H:i') }}
                </td>
                <td style="white-space:nowrap">
                    {{ $registro->deadline_vgm->format('d/m H:i') }}
                </td>
                <td>{{ $registro->terminal }}</td>
                <td>{{ $registro->pernada }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
    @endif

@endsection
