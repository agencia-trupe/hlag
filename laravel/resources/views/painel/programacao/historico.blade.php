@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.programacao.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Programação
    </a>

    <legend>
        <h2><small>Programação /</small> Histórico</h2>
    </legend>

    @if(!count($historico))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Arquivo</th>
                <th>Enviado em</th>
            </tr>
        </thead>

        <tbody>
        @foreach ($historico as $registro)
            <tr class="tr-row">
                <td style="width:100%">
                    <a href="{{ route('painel.programacao.historico.download', $registro->id) }}" target="_blank">
                        {{ $registro->arquivo }}
                    </a>
                </td>
                <td style="white-space:nowrap">
                    {{ $registro->created_at->format('d/m/Y H:i:s') }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $historico->render() !!}
    @endif

@endsection
