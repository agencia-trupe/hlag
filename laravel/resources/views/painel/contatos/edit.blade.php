@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Contatos /</small> Editar Divisão</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.contatos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.contatos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
