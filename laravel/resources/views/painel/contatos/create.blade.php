@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Contatos /</small> Adicionar Divisão</h2>
    </legend>

    {!! Form::open(['route' => 'painel.contatos.store', 'files' => true]) !!}

        @include('painel.contatos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
