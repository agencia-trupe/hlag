@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título [PT]') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('telefones_pt', 'Telefones [PT]') !!}
            {!! Form::text('telefones_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título [EN]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('telefones_en', 'Telefones [EN]') !!}
            {!! Form::text('telefones_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título [ES]') !!}
            {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('telefones_es', 'Telefones [ES]') !!}
            {!! Form::text('telefones_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.contatos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
