@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Contatos / {{ $divisao->titulo_pt }} /</small> Editar E-mail</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.contatos.emails.update', $divisao->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.contatos.emails.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
