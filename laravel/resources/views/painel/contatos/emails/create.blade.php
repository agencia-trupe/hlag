@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Contatos / {{ $divisao->titulo_pt }} /</small> Adicionar E-mail</h2>
    </legend>

    {!! Form::open(['route' => ['painel.contatos.emails.store', $divisao->id], 'files' => true]) !!}

        @include('painel.contatos.emails.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
