@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo_pt', 'Título [PT]') !!}
    {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título [EN]') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título [ES]') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.contatos.emails.index', $divisao->id) }}" class="btn btn-default btn-voltar">Voltar</a>
