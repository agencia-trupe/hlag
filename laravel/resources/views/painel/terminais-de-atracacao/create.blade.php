@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Terminais de Atracação /</small> Adicionar Terminal</h2>
    </legend>

    {!! Form::open(['route' => 'painel.terminais-de-atracacao.store', 'files' => true]) !!}

        @include('painel.terminais-de-atracacao.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
