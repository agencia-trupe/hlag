@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Terminais de Atracação /</small> Editar Terminal</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.terminais-de-atracacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.terminais-de-atracacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
