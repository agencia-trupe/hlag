@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Portos /</small> Adicionar Porto</h2>
    </legend>

    {!! Form::open(['route' => 'painel.portos.store', 'files' => true]) !!}

        @include('painel.portos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
