@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Portos /</small> Editar Porto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.portos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.portos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
