-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: 24-Maio-2019 às 17:38
-- Versão do servidor: 5.7.22
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hlag`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, 'Hapag-Lloyd', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `razao_social_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `razao_social_en` text COLLATE utf8_unicode_ci NOT NULL,
  `razao_social_es` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco_en` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco_es` text COLLATE utf8_unicode_ci NOT NULL,
  `telefone_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aviso_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aviso_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aviso_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `razao_social_pt`, `razao_social_en`, `razao_social_es`, `endereco_pt`, `endereco_en`, `endereco_es`, `telefone_pt`, `telefone_en`, `telefone_es`, `aviso_pt`, `aviso_en`, `aviso_es`, `created_at`, `updated_at`) VALUES
(1, 'Libra Serviços de Navegação Ltda c/o Hapag-Lloyd AG<br />CNPJ: 42.581.413.0001-57', 'Libra Serviços de Navegação Ltda c/o Hapag-Lloyd AG<br />CNPJ: 42.581.413.0001-57', 'Libra Serviços de Navegação Ltda c/o Hapag-Lloyd AG<br />CNPJ: 42.581.413.0001-57', 'Av. das Nações Unidas 14261 - Ala B - 28º andar - Bloco B<br />Vila Girtrudes - São Paulo/SP - 04794-000', 'Av. das Nações Unidas 14261 - Ala B - 28º andar - Bloco B<br />Vila Girtrudes - São Paulo/SP - 04794-000', 'Av. das Nações Unidas 14261 - Ala B - 28º andar - Bloco B<br />Vila Girtrudes - São Paulo/SP - 04794-000', '+55 11 4090-1555', '+55 11 4090-1555', '+55 11 4090-1555', 'A Hapag-Lloyd reserva-se o direito de alterar os dados aqui divulgados sem prévio aviso.', 'A Hapag-Lloyd reserva-se o direito de alterar os dados aqui divulgados sem prévio aviso.', 'A Hapag-Lloyd reserva-se o direito de alterar os dados aqui divulgados sem prévio aviso.', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_divisoes`
--

CREATE TABLE `contatos_divisoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos_divisoes`
--

INSERT INTO `contatos_divisoes` (`id`, `ordem`, `titulo_pt`, `titulo_en`, `titulo_es`, `telefones_pt`, `telefones_en`, `telefones_es`, `created_at`, `updated_at`) VALUES
(1, 0, 'Customer Service', 'Customer Service', 'Customer Service', 'Phone 4090 1555 / 0300 11 55 300', 'Phone 4090 1555 / 0300 11 55 300', 'Phone 4090 1555 / 0300 11 55 300', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_emails`
--

CREATE TABLE `contatos_emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `divisao_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos_emails`
--

INSERT INTO `contatos_emails` (`id`, `divisao_id`, `ordem`, `titulo_pt`, `titulo_en`, `titulo_es`, `email`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'Sales Support', 'Sales Support', 'Sales Support', 'sales.br@hlag.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2019_05_20_175042_create_servicos_table', 1),
('2019_05_20_175438_create_terminais_de_atracacao_table', 1),
('2019_05_20_180501_create_contatos_table', 1),
('2019_05_20_200159_create_programacao_table', 1),
('2019_05_20_200708_create_portos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `portos`
--

CREATE TABLE `portos` (
  `id` int(10) UNSIGNED NOT NULL,
  `sigla` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portos`
--

INSERT INTO `portos` (`id`, `sigla`, `titulo`, `created_at`, `updated_at`) VALUES
(1, 'BRSSZ', 'SANTOS', NULL, NULL),
(2, 'BRRIO', 'RIO DE JANEIRO', NULL, NULL),
(3, 'BRSSA', 'SALVADOR', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `programacao`
--

CREATE TABLE `programacao` (
  `id` int(10) UNSIGNED NOT NULL,
  `porto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `navio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `viagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `servico` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `previsao_atracacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `previsao_saida` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deadline_dca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deadline_mdgf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deadline_draft` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deadline_carga_liberacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deadline_vgm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `terminal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pernada` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `programacao`
--

INSERT INTO `programacao` (`id`, `porto`, `navio`, `viagem`, `servico`, `previsao_atracacao`, `previsao_saida`, `deadline_dca`, `deadline_mdgf`, `deadline_draft`, `deadline_carga_liberacao`, `deadline_vgm`, `terminal`, `pernada`, `created_at`, `updated_at`) VALUES
(1, 'SANTOS', 'FERNAO DE MAGALHAES', '057S', 'IRL', '2019-01-17 00:00:00', '2019-01-18 00:00:00', '2019-01-03 17:00:00', '2019-01-08 17:00:00', '2019-01-07 12:00:00', '2019-01-14 17:00:00', '2019-01-14 15:00:00', 'SANTOS BRASIL PARTICIPACOES S.A.', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(2, 'SANTOS', 'CAP SAN LAZARO', '848E', 'AS2', '2019-01-16 00:00:00', '2019-01-17 00:00:00', '2019-01-03 18:00:00', '2019-01-08 18:00:00', '2019-01-11 18:00:00', '2019-01-14 18:00:00', '2019-01-14 16:00:00', 'EMBRAPORT EMPRESA BRASILEIRA', 'E', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(3, 'SANTOS', 'NORTHERN DELEGATION', '60295Z', 'SAT', '2019-01-16 00:00:00', '2019-01-17 00:00:00', '2019-01-07 21:06:00', '2019-01-10 21:06:00', '2019-01-16 17:00:00', '2019-01-16 21:06:00', '2019-01-16 21:06:00', 'LIBRA TERMINAL 37', 'Z', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(4, 'SANTOS', 'FERNAO DE MAGALHAES', '057SS', 'IRL', '2019-01-17 00:00:00', '2019-01-18 00:00:00', '2019-01-03 17:00:00', '2019-01-08 17:00:00', '2019-01-07 12:00:00', '2019-01-14 17:00:00', '2019-01-14 15:00:00', 'SANTOS BRASIL PARTICIPACOES S.A.', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(5, 'SANTOS', 'MSC AGADIR', 'MM851A', 'MSE', '2019-01-17 00:00:00', '2019-01-18 00:00:00', '2019-01-03 12:00:00', '2019-01-08 12:00:00', '2019-01-11 18:00:00', '2019-01-14 12:00:00', '2019-01-14 10:00:00', 'BRASIL TERMINAL PORTUARIO SA', 'A', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(6, 'SANTOS', 'MONTE TAMARO', '092S', 'SEC', '2019-01-17 00:00:00', '2019-01-18 00:00:00', '2019-01-03 17:00:00', '2019-01-08 17:00:00', '2019-01-11 18:00:00', '2019-01-14 17:00:00', '2019-01-13 17:00:00', 'SANTOS BRASIL PARTICIPACOES S.A.', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(7, 'SANTOS', 'LOG-IN PANTANAL', '239N', 'IRL', '2019-01-16 00:00:00', '2019-01-18 00:00:00', '2019-01-07 15:30:00', '2019-01-10 15:30:00', '2019-01-16 18:00:00', '2019-01-16 15:30:00', '2019-01-16 13:30:00', 'EMBRAPORT EMPRESA BRASILEIRA', 'N', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(8, 'SANTOS', 'LOG-IN PANTANAL', '238S', 'IRL', '2019-01-16 00:00:00', '2019-01-18 00:00:00', '2019-01-07 15:30:00', '2019-01-10 15:30:00', '2019-01-16 18:00:00', '2019-01-16 15:30:00', '2019-01-16 13:30:00', 'EMBRAPORT EMPRESA BRASILEIRA', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(9, 'SANTOS', 'MSC ARICA', 'NA902R', 'ECX', '2019-01-17 00:00:00', '2019-01-18 00:00:00', '2019-01-07 12:00:00', '2019-01-10 12:00:00', '2019-01-14 18:00:00', '2019-01-16 12:00:00', '2019-01-16 10:00:00', 'BRASIL TERMINAL PORTUARIO SA', 'R', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(10, 'SANTOS', 'VALOR', '1308-024W', 'ASE', '2019-01-18 00:00:00', '2019-01-19 00:00:00', '2019-01-07 18:00:00', '2019-01-10 18:00:00', '2019-01-14 18:00:00', '2019-01-16 18:00:00', '2019-01-16 16:00:00', 'LIBRA TERMINAL 37', 'W', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(11, 'SANTOS', 'MSC LORETTA', '903N', 'SCS', '2019-01-18 00:00:00', '2019-01-19 00:00:00', '2019-01-08 18:00:00', '2019-01-11 18:00:00', '2019-01-16 18:00:00', '2019-01-17 18:00:00', '2019-01-17 16:00:00', 'SANTOS BRASIL PARTICIPACOES S.A.', 'N', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(12, 'SANTOS', 'CAP SAN NICOLAS', '901S', 'BPX', '2019-01-19 00:00:00', '2019-01-20 00:00:00', '2019-01-09 12:00:00', '2019-01-14 12:00:00', '2019-01-16 18:00:00', '2019-01-18 12:00:00', '2019-01-18 10:00:00', 'SANTOS BRASIL PARTICIPACOES S.A.', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(13, 'RIO DE JANEIRO', 'VALOR', '1308-024W', 'ASE', '2019-01-16 00:00:00', '2019-01-17 00:00:00', '2019-01-04 12:00:00', '2019-01-09 12:00:00', '2019-01-11 18:00:00', '2019-01-15 12:00:00', '2019-01-15 10:00:00', 'LIBRA TERMINAL RIO S/A', 'W', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(14, 'RIO DE JANEIRO', 'LOG-IN PANTANAL', '239N', 'IRL', '2019-01-19 00:00:00', '2019-01-19 00:00:00', '2019-01-09 11:00:00', '2019-01-14 11:00:00', '2019-01-18 11:00:00', '2019-01-18 11:00:00', '2019-01-18 09:00:00', 'LIBRA TERMINAL RIO S/A', 'N', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(15, 'RIO DE JANEIRO', 'LOG-IN PANTANAL', '238S', 'IRL', '2019-01-19 00:00:00', '2019-01-19 00:00:00', '2019-01-09 11:00:00', '2019-01-14 11:00:00', '2019-01-18 11:00:00', '2019-01-18 11:00:00', '2019-01-18 09:00:00', 'LIBRA TERMINAL RIO S/A', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(16, 'RIO DE JANEIRO', 'LOG-IN PANTANAL', '239NX', 'IRL', '2019-01-19 00:00:00', '2019-01-19 00:00:00', '2019-01-09 11:00:00', '2019-01-14 11:00:00', '2019-01-18 11:00:00', '2019-01-18 11:00:00', '2019-01-18 09:00:00', 'LIBRA TERMINAL RIO S/A', 'X', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(17, 'RIO DE JANEIRO', 'MSC ARICA', 'NA902R', 'ECX', '2019-01-19 00:00:00', '2019-01-19 00:00:00', '2019-01-08 17:00:00', '2019-01-11 17:00:00', '2019-01-15 18:00:00', '2019-01-17 17:00:00', '2019-01-17 15:00:00', 'MULTI-RIO OPERACOES PORTUARIAS S.A', 'R', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(18, 'RIO DE JANEIRO', 'NORDIC HONG KONG', '0013E', 'CON', '2019-01-19 00:00:00', '2019-01-20 00:00:00', '2019-01-09 12:00:00', '2019-01-14 12:00:00', '2019-01-16 12:00:00', '2019-01-18 12:00:00', '2019-01-18 10:00:00', 'LIBRA TERMINAL RIO S/A', 'E', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(19, 'RIO DE JANEIRO', 'NORDIC HONG KONG', '0903W', 'CON', '2019-01-19 00:00:00', '2019-01-20 00:00:00', '2019-01-09 12:00:00', '2019-01-14 12:00:00', '2019-01-16 12:00:00', '2019-01-18 12:00:00', '2019-01-18 10:00:00', 'LIBRA TERMINAL RIO S/A', 'W', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(20, 'RIO DE JANEIRO', 'MSC LORETTA', '903N', 'SCS', '2019-01-20 00:00:00', '2019-01-20 00:00:00', '2019-01-10 10:00:00', '2019-01-15 10:00:00', '2019-01-16 18:00:00', '2019-01-19 10:00:00', '2019-01-19 08:00:00', 'LIBRA TERMINAL RIO S/A', 'N', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(21, 'RIO DE JANEIRO', 'MSC LORETTA', '88S', 'SCS', '2019-01-20 00:00:00', '2019-01-20 00:00:00', '2019-01-10 10:00:00', '2019-01-15 10:00:00', '2019-01-16 18:00:00', '2019-01-19 10:00:00', '2019-01-19 08:00:00', 'LIBRA TERMINAL RIO S/A', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(22, 'SALVADOR', 'MONTE OLIVIA', '902N', 'SCS', '2019-01-16 00:00:00', '2019-01-17 00:00:00', '2019-01-04 18:00:00', '2019-01-09 18:00:00', '2019-01-11 18:00:00', '2019-01-15 18:00:00', '2019-01-15 16:00:00', 'TECON SALVADOR SA', 'N', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(23, 'SALVADOR', 'MONTE OLIVIA', '87S', 'SCS', '2019-01-16 00:00:00', '2019-01-17 00:00:00', '2019-01-04 18:00:00', '2019-01-09 18:00:00', '2019-01-11 18:00:00', '2019-01-15 18:00:00', '2019-01-15 16:00:00', 'TECON SALVADOR SA', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(24, 'SALVADOR', 'NORTHERN GUARD', '901N', 'SEC', '2019-01-16 00:00:00', '2019-01-17 00:00:00', '2019-01-07 18:00:00', '2019-01-10 18:00:00', '2019-01-14 18:00:00', '2019-01-16 18:00:00', '2019-01-16 16:00:00', 'TECON SALVADOR SA', 'N', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(25, 'SALVADOR', 'NORTHERN GUARD', '002S', 'SEC', '2019-01-16 00:00:00', '2019-01-17 00:00:00', '2019-01-07 18:00:00', '2019-01-10 18:00:00', '2019-01-14 18:00:00', '2019-01-16 18:00:00', '2019-01-16 16:00:00', 'TECON SALVADOR SA', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(26, 'SALVADOR', 'JOAO DE SOLIS', '902N', 'IRL', '2019-01-18 00:00:00', '2019-01-19 00:00:00', '2019-01-08 17:00:00', '2019-01-11 17:00:00', '2019-01-16 17:00:00', '2019-01-17 17:00:00', '2019-01-17 15:00:00', 'TECON SALVADOR SA', 'N', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(27, 'SALVADOR', 'MERCOSUL GUARANI', '007N', 'IRL', '2019-01-18 00:00:00', '2019-01-19 00:00:00', '2019-01-08 11:00:00', '2019-01-11 11:00:00', '2019-01-15 18:00:00', '2019-01-17 11:00:00', '2019-01-16 11:00:00', 'TECON SALVADOR SA', 'N', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(28, 'SALVADOR', 'MERCOSUL GUARANI', '007S', 'IRL', '2019-01-18 00:00:00', '2019-01-19 00:00:00', '2019-01-08 11:00:00', '2019-01-11 11:00:00', '2019-01-15 18:00:00', '2019-01-17 11:00:00', '2019-01-16 11:00:00', 'TECON SALVADOR SA', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(29, 'SALVADOR', 'MONTE SARMIENTO', '902N', 'IRL', '2019-01-20 00:00:00', '2019-01-20 00:00:00', '2019-01-09 17:00:00', '2019-01-14 17:00:00', '2019-01-16 18:00:00', '2019-01-18 17:00:00', '2019-01-17 17:00:00', 'TECON SALVADOR SA', 'N', '2019-05-24 17:37:32', '2019-05-24 17:37:32'),
(30, 'SALVADOR', 'MONTE SARMIENTO', '903S', 'IRL', '2019-01-20 00:00:00', '2019-01-20 00:00:00', '2019-01-09 17:00:00', '2019-01-14 17:00:00', '2019-01-16 18:00:00', '2019-01-18 17:00:00', '2019-01-17 17:00:00', 'TECON SALVADOR SA', 'S', '2019-05-24 17:37:32', '2019-05-24 17:37:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `programacao_historico`
--

CREATE TABLE `programacao_historico` (
  `id` int(10) UNSIGNED NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `programacao_historico`
--

INSERT INTO `programacao_historico` (`id`, `arquivo`, `created_at`, `updated_at`) VALUES
(1, 'deadline-extraction-br-all-ports_20190524173732p1ak31mbtT.xls', '2019-05-24 17:37:32', '2019-05-24 17:37:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE `servicos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `ordem`, `titulo_pt`, `titulo_en`, `titulo_es`, `link`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hapag-Lloyd', 'Hapag-Lloyd', 'Hapag-Lloyd', 'https://www.hapag-lloyd.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `terminais_de_atracacao`
--

CREATE TABLE `terminais_de_atracacao` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `terminais_de_atracacao`
--

INSERT INTO `terminais_de_atracacao` (`id`, `ordem`, `titulo_pt`, `titulo_en`, `titulo_es`, `link`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hapag-Lloyd', 'Hapag-Lloyd', 'Hapag-Lloyd', 'https://www.hapag-lloyd.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$NfG8GNOsFjWE14ws86..NuFFYgBTcbDHNzKjQhsnyi4esX1JJMDGu', '1rAknfySag4UsrRhXokganeQg71WAw8Owpmpa6mqM9BbBdNiaGXCZCobENBb', NULL, '2019-05-24 17:37:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_divisoes`
--
ALTER TABLE `contatos_divisoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_emails`
--
ALTER TABLE `contatos_emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contatos_emails_divisao_id_foreign` (`divisao_id`);

--
-- Indexes for table `portos`
--
ALTER TABLE `portos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `portos_sigla_unique` (`sigla`);

--
-- Indexes for table `programacao`
--
ALTER TABLE `programacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `programacao_historico`
--
ALTER TABLE `programacao_historico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicos`
--
ALTER TABLE `servicos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terminais_de_atracacao`
--
ALTER TABLE `terminais_de_atracacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contatos_divisoes`
--
ALTER TABLE `contatos_divisoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contatos_emails`
--
ALTER TABLE `contatos_emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `portos`
--
ALTER TABLE `portos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `programacao`
--
ALTER TABLE `programacao`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `programacao_historico`
--
ALTER TABLE `programacao_historico`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `servicos`
--
ALTER TABLE `servicos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `terminais_de_atracacao`
--
ALTER TABLE `terminais_de_atracacao`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `contatos_emails`
--
ALTER TABLE `contatos_emails`
  ADD CONSTRAINT `contatos_emails_divisao_id_foreign` FOREIGN KEY (`divisao_id`) REFERENCES `contatos_divisoes` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
